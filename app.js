var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
// var nodejieba = require("nodejieba");
var QuestionModel = require('./model/Question');
var AnswerModel = require('./model/Answer');
var KnowledgeModel = require('./model/Knowledge');
var ClassificationModel = require('./model/Classification');
var qcloud = require('qcloud_cos')

var app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json())


mongoose.connect(process.env.MONGOLAB_URI || 'mongodb://localhost:27017/tcqa');
var db = mongoose.connection;
db.on('error', function() {
    console.error('MongoDB Connection Error. Make sure MongoDB is running.');
});
db.once('open', function() {
    console.log('MongoDB Connection succeed.');
});

var server = app.listen(3000, function () {
    var port = server.address().port;
    console.log('TCQA app listening at port %s', port);	
});

app.post('/question/addone', function(req, res){
    var questionEntity = new QuestionModel({
    	Summary: req.body.summary,
    	Sdkappid: req.body.appid,
    	Avver: req.body.avver,
    	Plat: req.body.plat,
    	Detail: req.body.detail,
    });
    console.log(req.body);
    questionEntity.save(function(err){
    	if (err){
    		res.status(400);
    	}else{
    		res.status(200).json({code: 0});
    	}
    });

});

app.post('/question/list', function(req, res){
	QuestionModel.find().skip(req.body.page*req.body.size).limit(req.body.size).exec(function(err,datas){  
		if (datas){
			res.status(200).json(datas);
		}
	});
});

app.get('/question/size', function(req, res){
	QuestionModel.count({}, function( err, count){
    	res.status(200).json({size: count});
	});
});

app.post('/question/detail', function(req, res){
	QuestionModel.findOne({'_id':req.body.id}, function(err,data){
		var mData = {Summary: data.Summary, Sdkappid:data.Sdkappid, Avver:data.Avver, Plat:data.Plat, 
			Detail:data.Detail, Reply: []};
		var ids = [];		
		if (data.Reply != null){
			for (var i = 0; i < data.Reply.length; ++i){
				if (data.Reply[i].Knowledge != null && data.Reply[i].Knowledge.length > 0){
					for (var j = 0; j < data.Reply[i].Knowledge.length; ++j){
						ids.push(mongoose.Types.ObjectId(data.Reply[i].Knowledge[j]));
					}					
				}
			}
		}
		if (ids.length > 0){
			KnowledgeModel.find({
			    '_id': { $in: ids}
			}, function(err, docs){
				for (var i = 0; i < data.Reply.length; ++i){
					var mReply = {Content: data.Reply[i].Content, Knowledge: []};
					if (data.Reply[i].Knowledge != null && data.Reply[i].Knowledge.length > 0){					
						for (var j = 0; j < data.Reply[i].Knowledge.length; ++j){
							for (var k = 0; k < docs.length; ++k){
								if (docs[k]._id.equals(mongoose.Types.ObjectId(data.Reply[i].Knowledge[j]))){
									mReply.Knowledge.push(docs[k]);
								}
							}
						}					
					}
					mData.Reply.push(mReply);
				}
				res.status(200).json(mData);
			});
		}else{
			for (var i = 0; i < data.Reply.length; ++i){
				var mReply = {Content: data.Reply[i].Content, Knowledge: []};
				mData.Reply.push(mReply);
			}
			res.status(200).json(mData);
		}		

		
		// if (data.Answer != null){			
		// 	AnswerModel.findOne({_id: data.Answer}, function(err, ansdata){
		// 		var newData = {Summary: data.Summary, Sdkappid:data.Sdkappid, Avver:data.Avver, Plat:data.Plat, 
		// 			Detail:data.Detail, AnswerStr:ansdata.Detail, Answer:data.Answer};
		// 		data['AnswerStr'] = ansdata.Detail;
		// 		console.log(newData);
		// 		res.status(200).json(newData);
		// 	})
		// }else{
		// 	res.status(200).json(data);
		// }		
    	
	});
});

app.post('/answer/addone', function(req, res){
    var answerEntity = new AnswerModel({
    	Detail: req.body.detail,
    	Condition: '{平台:android, avver:1.8.1以下}'
    });
    answerEntity.save(function(err){
    	if (err){
    		res.status(400);
    	}else{
    		res.status(200).json({code: 0});
    	}
    });
});

app.get('/answer/list', function(req, res){
	AnswerModel.find().exec(function(err,datas){  
		if (datas){
			res.status(200).json(datas);
		}
	});
});

// app.post('/question/addreply', function(req, res){
// 	QuestionModel.update({'_id':req.body.questionId},{ $set: { Answer: mongoose.Types.ObjectId(req.body.answerId) }},{multi:true},function (err, numberAffected, raw){
// 		res.status(200).json({code: 0});
//     });

// });

app.post('/question/addreply', function(req, res){
	QuestionModel.update({'_id':req.body.questionId},{ $push:{ Reply: req.body.reply} },{},function (err, numberAffected, raw){
		res.status(200).json({code: 0});
    });

});


app.get('/knowledge/list', function(req, res){
	KnowledgeModel.find().exec(function(err,datas){  
		if (datas){
			res.status(200).json(datas);
		}
	});
});

app.post('/knowledge/find', function(req, res){
	var requestData = [];
	var request = {};
	if (req.body.errorcode && req.body.errorcode != 0){
		request.Code = req.body.errorcode;
	}
	if (req.body.plat && req.body.plat != '0'){
		request.Plat = req.body.plat;
	}
	if (req.body.classification){
		request.Classification = mongoose.Types.ObjectId(req.body.classification);
	}
	if (req.body.des && req.body.des != ''){
		request.Des = new RegExp(req.body.des); 
		 
	}

	requestData.push(request);	
	// if (req.body.des && req.body.des != ''){
	// 	var keyRequest = [];
	// 	var keyData = {}
	// 	keyData.Des = new RegExp(cutResult[i]);
	// 	keyRequest.push(keyData);

	// 	// var cutResult = nodejieba.cut(req.body.des);
	// 	// for (var i = 0; i < cutResult.length; ++i){
	// 	// 	var keyData = {}
	// 	// 	keyData.Des = new RegExp(cutResult[i]);
	// 	// 	keyRequest.push(keyData);
	// 	// }		
	// 	requestData.push({ $or: keyRequest });
	// }
	KnowledgeModel.find()
		.and(requestData)
		.exec(function(err,datas){  
			if (datas){
				res.status(200).json(datas);
			}else{
				console.log(err);
			}
	});
});


app.post('/knowledge/findtop', function(req, res){
	var requestData = [];
	var request = {};
	if (req.body.errorcode && req.body.errorcode != 0){
		request.Code = req.body.errorcode;
	}
	if (req.body.plat && req.body.plat != '0'){
		request.Plat = req.body.plat;
	}
	if (req.body.classification){
		request.Classification = mongoose.Types.ObjectId(req.body.classification);
	}
	// if (req.body.des && req.body.des != ''){
	// 	request.Des = new RegExp(req.body.des); 		
	// }
	requestData.push(request);	
	if (req.body.des && req.body.des != ''){
		var keyRequest = [];
		
		var keyData = {}
		keyData.Des = new RegExp(req.body.des);
		keyRequest.push(keyData);

		var keyData2 = {}
		keyData2.Content = new RegExp(req.body.des);
		keyRequest.push(keyData2);

		// for (var i = 0; i < cutResult.length; ++i){
		// 	var keyData = {}
		// 	keyData.Des = new RegExp(cutResult[i]);
		// 	keyRequest.push(keyData);
		// }		
		requestData.push({ $or: keyRequest });
	}
	KnowledgeModel.find()
		.and(requestData)
		.limit(5)
		.exec(function(err,datas){  
			if (datas){
				res.status(200).json(datas);
			}else{
				console.log(err);
			}
	});
});



app.post('/knowledge/addone', function(req, res){
    var knowledgeEntity = new KnowledgeModel({
    	Des: req.body.des,
    	Content: req.body.content,
    	Code: req.body.errorcode,
    	Plat: req.body.plat,
    	Classification: mongoose.Types.ObjectId(req.body.classification),	
    });
    knowledgeEntity.save(function(err){
    	if (err){
    		res.status(400);
    	}else{
    		res.status(200).json({code: 0});
    	}
    });
});

app.post('/knowledge/detail', function(req, res){
	var result = {};	
	KnowledgeModel.findOne({'_id':req.body.id}, function(err,data){
		result.Des = data.Des;
		result.Content = data.Content;
		result.Code = data.Code;
		result.Plat = data.Plat;
		if (data){
			ClassificationModel.findOne({'_id':data.Classification}, function(err, classdata){
				result.Classification = classdata.Des;
				res.status(200).json(result);
			});
			
		}
		
	});
});



app.post('/knowledge/addclass', function(req, res){	
	var classificationEntity = new ClassificationModel({
    	Des: req.body.des,    	  	
    });
    classificationEntity.save(function(err){
    	if (err){
    		res.status(400);
    	}else{
    		res.status(200).json({code: 0});
    	}
    });
});

app.get('/knowledge/getclasses', function(req, res){	
	ClassificationModel.find().exec(function(err,datas){  
		if (datas){
			res.status(200).json(datas);
		}
	});
});


app.get('/upload/getsig', function(req, res){	
	res.status(200).json({sign: qcloud.auth.signMore('tcqa', parseInt(Date.now()/1000) + 3600)});
});



