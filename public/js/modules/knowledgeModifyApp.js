var knowledgeModifyApp=angular.module('knowledgeModifyApp',['ngRoute']);

knowledgeModifyApp.config(function($routeProvider){
	$routeProvider.
	when('/',{
		templateUrl:'knowledgeAddTemp.html',
		controller:'knowledgeAddCtrl'
	}).
	when('/:id',{
		templateUrl:'knowledgeDetailTemp.html',
		controller:'knowledgeDetailCtrl'
	}).
	otherwise({
		redirectTo:'/'
	})
});

knowledgeModifyApp.controller('knowledgeAddCtrl',function($scope, $http, $window){
	$scope.knowledge = {des:'', content:'', plat: '0', errorcode: 0, classification: ''};
	$scope.newClassification = "";
	$scope.classes = [];
	$scope.relates = [];
	$scope.addClassification = function(){
		if ($scope.newClassification === ""){
			return;
		}
		$http.post('/knowledge/addclass',{des: $scope.newClassification}).
			success(function(data,status,headers,config){
				$scope.newClassification = "";
				$scope.getClasses();
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});

	};
	$scope.getClasses = function(){
		$http.get('/knowledge/getclasses').
			success(function(data,status,headers,config){				
				$scope.classes = data;							
			}).
			error(function(data,status,headers,config){
			});
	};
	$scope.search = function(){		
		request = {};
		request.des = $scope.knowledge.des;
		request.plat = $scope.knowledge.plat;
		request.errorcode = $scope.knowledge.errorcode;	
		request.classification = $scope.knowledge.classification;
		console.log(request);	
		$http.post('/knowledge/find', request).
			success(function(data,status,headers,config){	
				$scope.relates = data;					
			}).
			error(function(data,status,headers,config){
			});
	}
	$scope.submit = function(){
  		$http.post('/knowledge/addone',$scope.knowledge).
			success(function(data,status,headers,config){
				alert('send succeed');
				$scope.knowledge.des = '';
				$scope.knowledge.content = '';	
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});
  	};
  	$scope.upload = function(){
  		var cos = new CosCloud('10022853');
  		$http.get('/upload/getsig').
			success(function(data,status,headers,config){
				console.log(data);
				var files = document.getElementById("file").files;				
				if(files && files.length == 1){
					cos.uploadFile(data.sign, function(result){
						var json = $scope.$eval(result);
						if (json.code === 0){
							var content = document.getElementById("content");
							$scope.knowledge.content = $scope.knowledge.content + '![](' + json.data.access_url + ')';
							content.value = $scope.knowledge.content;
							preview.innerHTML = markdown.toHTML($scope.knowledge.content);
						}else{
							alert(json.code);
							console.log(json);
						}						
					},
					function(){
						console.log('error');
					},'tcqa', "/knowledge/" + files[0].name, files[0]);
				}
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});
  	};
  	$scope.showMarkdown = function(){
  		var preview = document.getElementById("preview");
  		preview.innerHTML = markdown.toHTML($scope.knowledge.content);
  	};
	var init = function () {
		$scope.getClasses();
	};
	init();

});


knowledgeModifyApp.controller('knowledgeDetailCtrl',function($scope, $http, $window, $routeParams){
	$scope.request = {id : $routeParams.id};
	$scope.getDetail = function(){
		console.log($scope.request);
		$http.post('/knowledge/detail', $scope.request).
			success(function(data,status,headers,config){				
				$scope.knowledge = data;

				showContent($scope.knowledge.Content);			
			}).
			error(function(data,status,headers,config){
			});
	};
	var init = function () {
		$scope.getDetail();
		
	};
	var showContent = function(contentStr){
		var content = document.getElementById("content");
		content.innerHTML = markdown.toHTML(contentStr);
	}
	init();
});