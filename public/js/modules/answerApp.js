var answerApp=angular.module('answerApp',['ngRoute']);

answerApp.config(function($routeProvider){
	$routeProvider.
	when('/',{
		templateUrl:'answerListTemp.html',
		controller:'answerListCtrl'
	}).
	when('/add',{
		templateUrl:'answerAddTemp.html',
		controller:'answerAddCtrl'
	}).
	otherwise({
		redirectTo:'/'
	})
});

answerApp.controller('answerListCtrl',function($scope, $http, $window){
	$scope.answers = [];
	var init = function () {
		$http.get('/answer/list').
			success(function(data,status,headers,config){				
				$scope.answers = data;				
			}).
			error(function(data,status,headers,config){
			});
	};
	init();
});

answerApp.controller('answerAddCtrl',function($scope, $http, $window){
	$scope.answer = {detail:''};
	$scope.submit = function(){  			
  		$http.post('/answer/addone',{detail:$scope.answer.detail}).
			success(function(data,status,headers,config){				
				alert('提交成功');
				$scope.answer.detail = '';																			
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});
  	}

});