var questionApp=angular.module('questionApp',['ngRoute']);

questionApp.config(function($routeProvider){
	$routeProvider.
	when('/',{
		templateUrl:'question.html',
		controller:'questionCtrl'
	}).
	when('/knowledge/:id',{
		templateUrl:'knowledgeDetailTemp.html',
		controller:'knowledgeDetailCtrl'
	}).
	when('/:id',{
		templateUrl:'questionDetailTemp.html',
		controller:'questionDetailCtrl'
	}).
	when('/:id/:knowledgeid',{
		templateUrl:'questionKnowledgeTemp.html',
		controller:'questionKnowledgeDetailCtrl'
	}).
	otherwise({
		redirectTo:'/'
	})
});

questionApp.factory('questionService', function($http){		
	var currentPage = 0;
	return{
		getCurrentPage: function(callback){
			callback(currentPage);
		},

	};

});

questionApp.controller('questionCtrl',function($scope, $http, $window){
	$scope.relates = [];
	$scope.question = {summary:'', appid:'', avver:'', plat:'', detail:'', errorcode: 0};
	$scope.submit = function(){  			
  		$http.post('/question/addone',{summary:$scope.question.summary, appid:$scope.question.appid, avver:$scope.question.avver, plat:$scope.question.plat, detail:$scope.question.detail}).
			success(function(data,status,headers,config){				
				alert('提交成功');
				$scope.question.summary = $scope.question.appid = $scope.question.avver = $scope.question.plat = $scope.question.detail = '';																			
			}).
			error(function(data,status,headers,config){
				console.log('signup callback error'+data);
			});
  	}
  	$scope.search = function(){		
		request = {};
		request.des = $scope.question.summary;
		request.plat = $scope.question.plat;
		request.errorcode = $scope.question.errorcode;	
		console.log(request);	
		$http.post('/knowledge/find', request).
			success(function(data,status,headers,config){	
				$scope.relates = data;					
			}).
			error(function(data,status,headers,config){
			});
	}
});

questionApp.controller('questionKnowledgeDetailCtrl',function($scope, $http, $window, $routeParams){
	$http.post('/knowledge/detail',{id: $routeParams.knowledgeid}).
		success(function(data,status,headers,config){				
			$scope.data = data;	
		}).
		error(function(data,status,headers,config){
		});
});

questionApp.controller('questionListCtrl',function($scope, $http, $window, questionService){
	var pageSize = 5;
	var currentPage = 0;
	var totalPage = 0;
	$scope.prevDisable=true;
	$scope.nextDisable=true;
	$scope.questions = [];
	var getList = function(page){ 
  		$http.post('/question/list',{page: page, size: pageSize}).
			success(function(data,status,headers,config){
				$scope.questions = data;
				$scope.nextDisable = currentPage+1 == totalPage;
				$scope.prevDisable = currentPage == 0;																			
			}).
			error(function(data,status,headers,config){
			});
  	};
  	$scope.getNext = function(){ 
  		getList(++currentPage);  		
  	};
  	 $scope.getPrev = function(){ 
  		getList(--currentPage);  		
  	};
	var init = function () {
		$http.get('/question/size').
			success(function(data,status,headers,config){
				totalPage = Math.ceil(data.size/pageSize);
				$scope.nextDisable = currentPage+1 == totalPage;
			}).
			error(function(data,status,headers,config){
			});
		getList(currentPage);
		questionService.getCurrentPage(function(current){
			// console.log(current);
		});
	};
	init();
});

questionApp.controller('questionDetailCtrl',function($scope, $http, $window, $routeParams){	
	$scope.qid = $routeParams.id;
	$scope.reply = '';
	$scope.appends = [];
	$scope.knowledges = [];
	$scope.submit = function(){ 
		var replyData = {Content: $scope.reply, Knowledge:[]};
		for (var i = 0; i < $scope.appends.length; i++){
			replyData.Knowledge.push($scope.appends[i]._id);
		}		
  		$http.post('/question/addreply',{questionId: $routeParams.id, reply: replyData}).
			success(function(data,status,headers,config){				
				// $route.reload();																		
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});
  	}
  	$scope.appendKnowledge = function(knowledge){  		
  		for (var i = 0; i < $scope.appends.length; ++i){
  			if (knowledge._id === $scope.appends[i]._id){
  				$scope.appends.splice(i, 1);
  				return;
  			}
  		}
  		$scope.appends.push(knowledge);
  	}
	var init = function () {
		$http.post('/question/detail',{id: $routeParams.id}).
			success(function(data,status,headers,config){				
				$scope.data = data;	
			}).
			error(function(data,status,headers,config){
			});
		$http.get('/knowledge/list').
			success(function(data,status,headers,config){	
				$scope.knowledges = data;						
			}).
			error(function(data,status,headers,config){
			});
	};
	init();
});

questionApp.controller('knowledgeDetailCtrl',function($scope, $http, $window, $routeParams){
	$scope.request = {id : $routeParams.id};
	$scope.getDetail = function(){
		console.log($scope.request);
		$http.post('/knowledge/detail', $scope.request).
			success(function(data,status,headers,config){				
				$scope.knowledge = data;

				showContent($scope.knowledge.Content);			
			}).
			error(function(data,status,headers,config){
			});
	};
	var init = function () {
		$scope.getDetail();
		
	};
	var showContent = function(contentStr){
		var content = document.getElementById("content");
		content.innerHTML = markdown.toHTML(contentStr);
	}
	init();
});