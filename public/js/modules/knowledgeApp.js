var knowledgeApp=angular.module('knowledgeApp',['ngRoute']);

knowledgeApp.config(function($routeProvider){
	$routeProvider.
	when('/',{
		templateUrl:'knowledgeListTemp.html',
		controller:'knowledgeListCtrl'
	}).
	when('/add',{
		templateUrl:'knowledgeAddTemp.html',
		controller:'knowledgeAddCtrl'
	}).
	when('/:id',{
		templateUrl:'knowledgeDetailTemp.html',
		controller:'knowledgeDetailCtrl'
	}).
	otherwise({
		redirectTo:'/'
	})
});



knowledgeApp.controller('knowledgeListCtrl',function($scope, $http, $window){
	$scope.knowledges = [];
	$scope.classes = [];
	$scope.key = '';
	$scope.request = {code: 0, plat: '0', key: ''};
	$scope.getList = function(){
		$http.get('/knowledge/getclasses').
			success(function(data,status,headers,config){				
				$scope.classes = data;	

			}).
			error(function(data,status,headers,config){
			});

	};
	$scope.getKnowledges = function(classification){
		console.log(classification);
		request = {};
		request.classification = classification;
		$http.post('/knowledge/find', request).
			success(function(data,status,headers,config){				
				$scope.knowledges = data;	

			}).
			error(function(data,status,headers,config){
			});

	};
	$scope.searchKnowledge = function(){		
		request = {};
		if ($scope.key !== ''){
			request.des = $scope.key;
		}		
		$http.post('/knowledge/findtop', request).
			success(function(data,status,headers,config){	
				console.log(data);
				$scope.knowledges = data;					
			}).
			error(function(data,status,headers,config){
			});
	};
	var init = function () {
		$scope.searchKnowledge();
	};
	init();
});

knowledgeApp.controller('knowledgeAddCtrl',function($scope, $http, $window){
	$scope.knowledge = {des:'', content:'', plat: '0'};
	$scope.submit = function(){
		// var tags = $scope.knowledge.tag.split(" ");
		// $scope.knowledge.tag = tags;
  		$http.post('/knowledge/addone',$scope.knowledge).
			success(function(data,status,headers,config){
				alert('send succeed');
				$scope.knowledge.des = '';
				$scope.knowledge.content = '';	
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});
  	};
  	$scope.upload = function(){
  		var cos = new CosCloud('10022853');
  		$http.get('/upload/getsig').
			success(function(data,status,headers,config){
				console.log(data);
				var files = document.getElementById("file").files;				
				if(files && files.length == 1){
					cos.uploadFile(data.sign, function(result){
						var json = $scope.$eval(result);
						if (json.code === 0){
							var content = document.getElementById("content");
							$scope.knowledge.content = $scope.knowledge.content + '![](' + json.data.access_url + ')';
							content.value = $scope.knowledge.content;
							preview.innerHTML = markdown.toHTML($scope.knowledge.content);
						}else{
							alert(json.code);
							console.log(json);
						}						
					},
					function(){
						console.log('error');
					},'tcqa', "/knowledge/" + files[0].name, files[0]);
				}
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});
  	};
  	$scope.showMarkdown = function(){
  		var preview = document.getElementById("preview");
  		preview.innerHTML = markdown.toHTML($scope.knowledge.content);
  	};
});

knowledgeApp.controller('knowledgeDetailCtrl',function($scope, $http, $window, $routeParams){
	$scope.request = {id : $routeParams.id};
	$scope.getDetail = function(){
		console.log($scope.request);
		$http.post('/knowledge/detail', $scope.request).
			success(function(data,status,headers,config){				
				$scope.knowledge = data;
				showContent($scope.knowledge.Content);			
			}).
			error(function(data,status,headers,config){
			});
	};
	var init = function () {
		$scope.getDetail();
		
	};
	var showContent = function(contentStr){
		var content = document.getElementById("content");
		content.innerHTML = markdown.toHTML(contentStr);
	}
	init();
});



