var questionManageApp=angular.module('questionManageApp',['ngRoute']);

questionManageApp.config(function($routeProvider){
	$routeProvider.
	when('/',{
		templateUrl:'questionListTemp.html',
		controller:'questionListCtrl'
	}).
	when('/:id',{
		templateUrl:'questionDetailTemp.html',
		controller:'questionDetailCtrl'
	}).
	when('/:id/:knowledgeid',{
		templateUrl:'questionKnowledgeTemp.html',
		controller:'questionKnowledgeDetailCtrl'
	}).
	otherwise({
		redirectTo:'/'
	})
});

questionManageApp.factory('questionService', function($http){		
	var currentPage = 0;
	return{
		getCurrentPage: function(callback){
			callback(currentPage);
		},

	};

});



questionManageApp.controller('questionKnowledgeDetailCtrl',function($scope, $http, $window, $routeParams){
	$http.post('/knowledge/detail',{id: $routeParams.knowledgeid}).
		success(function(data,status,headers,config){				
			$scope.data = data;	
		}).
		error(function(data,status,headers,config){
		});
});

questionManageApp.controller('questionListCtrl',function($scope, $http, $window, questionService){
	var pageSize = 5;
	var currentPage = 0;
	var totalPage = 0;
	$scope.prevDisable=true;
	$scope.nextDisable=true;
	$scope.questions = [];
	var getList = function(page){ 
  		$http.post('/question/list',{page: page, size: pageSize}).
			success(function(data,status,headers,config){
				$scope.questions = data;
				$scope.nextDisable = currentPage+1 == totalPage;
				$scope.prevDisable = currentPage == 0;																			
			}).
			error(function(data,status,headers,config){
			});
  	};
  	$scope.getNext = function(){ 
  		getList(++currentPage);  		
  	};
  	 $scope.getPrev = function(){ 
  		getList(--currentPage);  		
  	};
	var init = function () {
		$http.get('/question/size').
			success(function(data,status,headers,config){
				totalPage = Math.ceil(data.size/pageSize);
				$scope.nextDisable = currentPage+1 == totalPage;
			}).
			error(function(data,status,headers,config){
			});
		getList(currentPage);
		questionService.getCurrentPage(function(current){
			// console.log(current);
		});
	};
	init();
});

questionManageApp.controller('questionDetailCtrl',function($scope, $http, $window, $routeParams){	
	$scope.qid = $routeParams.id;
	$scope.reply = '';
	$scope.appends = [];
	$scope.knowledges = [];
	$scope.submit = function(){ 
		var replyData = {Content: $scope.reply, Knowledge:[]};
		for (var i = 0; i < $scope.appends.length; i++){
			replyData.Knowledge.push($scope.appends[i]._id);
		}		
  		$http.post('/question/addreply',{questionId: $routeParams.id, reply: replyData}).
			success(function(data,status,headers,config){				
				// $route.reload();																		
			}).
			error(function(data,status,headers,config){
				console.log('callback error'+data);
			});
  	}
  	$scope.appendKnowledge = function(knowledge){  		
  		for (var i = 0; i < $scope.appends.length; ++i){
  			if (knowledge._id === $scope.appends[i]._id){
  				$scope.appends.splice(i, 1);
  				return;
  			}
  		}
  		$scope.appends.push(knowledge);
  	}
	var init = function () {
		$http.post('/question/detail',{id: $routeParams.id}).
			success(function(data,status,headers,config){				
				$scope.data = data;	
			}).
			error(function(data,status,headers,config){
			});
		$http.get('/knowledge/list').
			success(function(data,status,headers,config){	
				$scope.knowledges = data;						
			}).
			error(function(data,status,headers,config){
			});
	};
	init();
});

questionManageApp.controller('knowledgeDetailCtrl',function($scope, $http, $window, $routeParams){
	$scope.request = {id : $routeParams.id};
	$scope.getDetail = function(){
		console.log($scope.request);
		$http.post('/knowledge/detail', $scope.request).
			success(function(data,status,headers,config){				
				$scope.knowledge = data;

				showContent($scope.knowledge.Content);			
			}).
			error(function(data,status,headers,config){
			});
	};
	var init = function () {
		$scope.getDetail();
		
	};
	var showContent = function(contentStr){
		var content = document.getElementById("content");
		content.innerHTML = markdown.toHTML(contentStr);
	}
	init();
});