
var mongoose = require('mongoose');

var Knowledge = new mongoose.Schema({    
    Des: {
        type: String
    },
    Content: {
        type: String
    },
    Code: {
    	type: Number
    },
    Plat: {
    	type: String
    },
    Classification: {
        type: mongoose.Schema.ObjectId
    }  
});



module.exports = mongoose.model('Knowledge', Knowledge);