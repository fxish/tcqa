

var mongoose = require('mongoose');

var Question = new mongoose.Schema({
    Summary: {
        type: String,
        required: true
    },
    Sdkappid: {
        type: String,
        required: true
    },
    Avver: {
        type: String,
        required: true
    },
    Plat: {
        type: String
    },
    Detail: {
        type: String
    },
    Code: {
        type: Number
    },
    Answer: {
        type: mongoose.Schema.ObjectId
    },
    Reply: [{
        Content: String,
        Knowledge: [mongoose.Schema.ObjectId],
    }], 
});



module.exports = mongoose.model('Question', Question);